#include "include/DataDistributor.h"

Core::DataDistributor::DataDistributor()
	: seed(1111)
{
	dataArray = std::make_unique<DataVector>();
	trainingData = std::make_shared<DataVector>();
	testData = std::make_shared<DataVector>();
	validationData = std::make_shared<DataVector>();
}

Core::DataDistributor::~DataDistributor()
{
}

void Core::DataDistributor::ReadFeatureVector(const std::string& path)
{
	// Magic Number, Number of images, Row size, Column size
	std::array<uint32_t, 4> header;
	unsigned char bytes[4]; // std::array<unsigned char, 4> bytes;
	std::ifstream file;
	file.open(path, std::ios::in | std::ios::binary);

	assert((file.is_open() && "Unable to read feature vector from given path."));

	for (size_t i = 0; i < 4; i++)
	{
		file.read((char*)&bytes, sizeof(bytes));
		header[i] = ConvertToLittleEndian(bytes);
	}
	std::cout << "Done getting input file header" << std::endl;
	
	const auto imageSize = header[2] * header[3]; // row * col
	const auto numImages = header[1];

	std::cout << "numImages = " << numImages << "\n";

	// For each image
	for (size_t i = 0; i < numImages; i++)
	{
		Core::Data data;
		uint8_t pixel;
		// char* el;

		for (size_t j = 0; j < imageSize; j++)
		{
			file.read((char*)&pixel, sizeof(pixel));
			data.AppendToFeatureVector(pixel);
		}

		dataArray->push_back(data);
	}

	file.close();
	std::cout
		<< "Successfully read and stored "
		<< dataArray->size()
		<< " feature vectors"
		<< std::endl;
}

void Core::DataDistributor::ReadFeatureLabels(const std::string& path)
{
	// magic numbers, images
	std::array<uint32_t, 2> header;
	std::array<unsigned char, 4> bytes; 
	// unsigned char bytes[4];

	std::ifstream file(path, std::ios::in | std::ios::binary);

	assert((file.is_open() && "Unable to read feature labels from given path"));

	for (int i = 0; i < 2; i++)
	{
		file.read((char*)& bytes, sizeof(bytes));
		header[i] = ConvertToLittleEndian(bytes);
	}

	std::cout << "Done getting label file header." << std::endl;
	for (size_t i = 0; i < header[1]; i++)
	{
		uint8_t label = 0;
		file.read((char*)& label, sizeof(label));
		dataArray->at(i).SetLabel(label);
	}

	file.close();
}

void Core::DataDistributor::SetSeed(const int seed)
{
	this->seed = seed;
}

void Core::DataDistributor::DistributeData(
	const int vectorSize,
	std::shared_ptr<DataVector> target,
	std::uniform_int_distribution<>& distributer,
	std::mt19937& generator,
	std::unordered_set<int>& usedIndexes)
{
	int count = 0;
	while (count < vectorSize)
	{
		int randomIndex = distributer(generator);
		bool indexNotFound = usedIndexes.find(randomIndex) == usedIndexes.end();
		if (indexNotFound)
		{
			usedIndexes.insert(randomIndex);
			target->push_back(std::move(dataArray->at(randomIndex)));
			count++;
		}
	}
}

void Core::DataDistributor::SplitData()
{
	std::unordered_set<int> usedIndexes;
	size_t dataSize = dataArray->size();
	int trainSize = static_cast<int> (dataSize * trainSetPercent);
	int testSize = static_cast<int> (dataSize * testSetPercent);
	int validationSize = static_cast<int> (dataSize * validationSetPercent);

	// const int seed = 1111;
	// std::random_device randDevice;
	std::mt19937 generator(seed); // generator(randDevice())
	std::uniform_int_distribution<> distribution(0, static_cast<int> (dataSize - 1));

	DistributeData(trainSize, trainingData, distribution, generator, usedIndexes);
	DistributeData(testSize, testData, distribution, generator, usedIndexes);
	DistributeData(validationSize, validationData, distribution, generator, usedIndexes);

	std::cout
		<< "Training data size: \t" << trainingData->size() << '\n'
		<< "Test data size: \t" << testData->size() << '\n'
		<< "Validation data size: \t" << validationData->size() << '\n'
		<< "Seed: \t\t\t" << seed
		<< std::endl;
}

void Core::DataDistributor::CountClasses()
{
	int count = 0;
	for (size_t i = 0; i < dataArray->size(); i++)
	{
		uint16_t label = dataArray->at(i).GetLabel();
		if (classMap.find(label) == classMap.end())
		{
			classMap[label] = count;
			dataArray->at(i).SetEnumeratedLabel(count);
			count++;
		}
	}

	numClasses = count;
	std::cout << "Successfully Extracted " << numClasses << " unique classes." << std::endl;
}

uint32_t Core::DataDistributor::ConvertToLittleEndian(const unsigned char* bytes)
{
	return static_cast<uint32_t> (
		(bytes[0] << 24) |
		(bytes[1] << 16) |
		(bytes[2] << 8) |
		(bytes[3]));
}

uint32_t Core::DataDistributor::ConvertToLittleEndian(const std::array<unsigned char, 4> bytes)
{
	return static_cast<uint32_t> (
		(bytes[0] << 24) |
		(bytes[1] << 16) |
		(bytes[2] << 8) |
		(bytes[3]));
}

std::shared_ptr<Core::DataDistributor::DataVector> Core::DataDistributor::GetTrainingData()
{
	return trainingData;
}

std::shared_ptr<Core::DataDistributor::DataVector> Core::DataDistributor::GetTestData()
{
	return testData;
}

std::shared_ptr<Core::DataDistributor::DataVector> Core::DataDistributor::GetValidationData()
{
	return validationData;
}
#pragma once

#include<vector>
#include<memory>
#include<iostream>

namespace Core
{
	class Data
	{
	public:
		Data();
		~Data();

		void SetDistance(double);

		void SetLabel(uint8_t);
		uint16_t GetLabel() const;

		void SetEnumeratedLabel(int);
		uint8_t GetEnumeratedLabel() const;

		void SetFeatureVector(std::vector<uint8_t>&);
		std::vector<uint8_t>& GetFeatureVector();
		size_t GetFeatureVectorSize();
		void AppendToFeatureVector(uint8_t);

	private:
		std::vector<uint8_t> featureVector;
		uint16_t label;
		int enumLabel;
		double distance;
	};
}



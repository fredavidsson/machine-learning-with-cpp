#pragma once
#include<memory>
#include<random>
#include<vector>
#include<string>
#include<fstream>
#include<map>
#include<unordered_set>
#include<iostream>
#include<cassert>
#include<array>
#include "Data.h"

namespace Core
{
	class DataDistributor
	{
	public:
		typedef std::vector<Data> DataVector;

		DataDistributor();
		~DataDistributor();

		void ReadFeatureVector(const std::string& path);
		void ReadFeatureLabels(const std::string& path);
		void SetSeed(const int seed);
		void SplitData();
		void CountClasses();

		uint32_t ConvertToLittleEndian(const unsigned char* bytes);
		uint32_t ConvertToLittleEndian(const std::array<unsigned char, 4> bytes);

		std::shared_ptr<DataVector> GetTrainingData();
		std::shared_ptr<DataVector> GetTestData();
		std::shared_ptr<DataVector> GetValidationData();

	private:
		std::unique_ptr<DataVector> dataArray; // All data pre-split

		std::shared_ptr<DataVector> trainingData;
		std::shared_ptr<DataVector> testData;
		std::shared_ptr<DataVector> validationData;

		int numClasses;
		int featureVectorSize;
		int seed;
		std::map<uint16_t, int> classMap;

		const double trainSetPercent = 0.750;
		const double testSetPercent = 0.20;
		const double validationSetPercent = 0.050;

		void DistributeData(
			const int vectorSize,
			std::shared_ptr<DataVector> target,
			std::uniform_int_distribution<>& distributer,
			std::mt19937& generator,
			std::unordered_set<int>& usedIndexes);
	};
}



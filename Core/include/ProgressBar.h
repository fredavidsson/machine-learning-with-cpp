#include<iostream>
#include<cassert>
#pragma once
namespace Core
{
	class ProgressBar
	{
	public:
		ProgressBar();
		void SetFraction(const double fraction);
		void SetMax(const size_t max);
		void Update();
		void ResetProgress();

		bool enabled;
	private:
		size_t fraction;
		size_t progress;
		size_t max;

		void PrintProgress();
	};
}


#include "include/ProgressBar.h"

Core::ProgressBar::ProgressBar()
	: fraction(0)
	, progress(0)
	, max(1)
	, enabled(true)
{
}

void Core::ProgressBar::SetFraction(const double fraction)
{
	this->fraction = static_cast<size_t>(fraction);
}

void Core::ProgressBar::SetMax(const size_t max)
{
	this->max = max;
	assert((max != 0 && "Division by zero error."));
}

void Core::ProgressBar::Update()
{
	if (!enabled)
	{
		return;
	}

	progress += 1;
	if (progress % fraction == 0)
	{
		PrintProgress();
	}
}

void Core::ProgressBar::ResetProgress()
{
	progress = 0;
}

void Core::ProgressBar::PrintProgress()
{
	std::cout << (progress * 100) / max;
	if (progress >= max)
	{
		std::cout << "%\n";
	}
	else
	{
		std::cout << "% ";
	}
}
#include "include/Data.h"

Core::Data::Data()
	: label(-1)
	, featureVector()
	, enumLabel(-1)
	, distance(0.0)
{
}

Core::Data::~Data()
{

}


void Core::Data::SetDistance(double value)
{
	distance = value;
}

void Core::Data::SetFeatureVector(std::vector<uint8_t>& vector)
{
	featureVector = std::move(vector);
}

std::vector<uint8_t>& Core::Data::GetFeatureVector()
{
	return featureVector;
}

size_t Core::Data::GetFeatureVectorSize()
{
	return featureVector.size();
}

void Core::Data::AppendToFeatureVector(uint8_t value)
{
	featureVector.push_back(value);
}

void Core::Data::SetLabel(uint8_t value)
{
	label = value;
}

uint16_t Core::Data::GetLabel() const
{
	return label;
}

void Core::Data::SetEnumeratedLabel(int value)
{
	enumLabel = value;
}

uint8_t Core::Data::GetEnumeratedLabel() const
{
	return enumLabel;
}
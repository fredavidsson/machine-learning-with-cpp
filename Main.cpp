#include "Core/include/Data.h"
#include "Core/include/DataDistributor.h"
#include "ML/include/KNN.h"
int main()
{
	Core::DataDistributor dataDistributor;
	dataDistributor.ReadFeatureVector("./dataset/train-images-idx3-ubyte");
	dataDistributor.ReadFeatureLabels("./dataset/train-labels-idx1-ubyte");
	// dataDistributor.SetSeed(1337); // Default is set to 1111 if this is not called.
	dataDistributor.SplitData();
	dataDistributor.CountClasses();

	ML::KNN kNearest;
	kNearest.SetTrainingData(dataDistributor.GetTrainingData());
	kNearest.SetTestData(dataDistributor.GetTestData());
	kNearest.SetValidationData(dataDistributor.GetValidationData());
	kNearest.SetNumThreads(24);

	// Enabled by default.
	// kNearest.progressBar.enabled = false; 

	double performance = 0.0;
	double bestPerformance = 0.0;
	int bestK = 1;

	std::cout << "This may take some time.." << std::endl;
	for (int i = 1; i <= 3; i++)
	{
		if (i == 1)
		{
			kNearest.SetK(i);
			performance = kNearest.ValidatePerformance();
			bestPerformance = performance;
		}
		else
		{
			kNearest.SetK(i);
			performance = kNearest.ValidatePerformance();
			if (performance > bestPerformance)
			{
				bestPerformance = performance;
				bestK = i;
			}
		}
	}

	std::cout
		<< "Best K: " << bestK << '\n'
		<< "Testing performance ... "
		<< std::endl;
	kNearest.SetK(bestK);
	kNearest.TestPerformance();
	return 0;
}
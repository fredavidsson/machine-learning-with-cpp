# Machine Learning with C++

This is just some project I've been working on for fun on my free time. My plan of this project is to write machine learning (ML) algorithms "from scratch" and apply these methods on the [MNIST database of handwritten digits](https://yann.lecun.com/exdb/mnist/), see [Wikipedia aritcle here](https://en.wikipedia.org/wiki/MNIST_database). 

## Features
Right now there is only one ML algorthm, the KNN method. See the [Wikipedia article on KNN here](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm). 

### Todo
- [ ] Add more than one norm to calculate distances. 
Currently the ```ML::KNN::CalculateDistance``` function is using the regular Euclidian norm to calculate the distance between points. It would be fun to add other distance functions, like the Manhattan norm for example.
- [X] Re-write the KNN algorithms so that it uses concurrent programming. 
- [ ] Implement the K-Means Algorithm.
- [ ] Start writing some results.

### And maybe later adding the following

- [ ] Gradient Decent.
- [ ] Linear Regression.
- [ ] Logistic Regression.
- [ ] OpenCL GPU acceleration

## Updates & notes
- 2022-12-19
	- Added a SetSeed function.
	- Added a progress bar for NKK, this can be disabled by calling ```kNearest.progressBar.enabled = false``` in ```main.cpp```
- 2022-12-12
    - Added multithreading support for the KNN algorithm.
- 2022-12-09
    - Changing from ```pow(x - y, 2)``` to ```(x - y) * (x - y)``` improved performance a lot. For ```k = 1```, finding the shortest distance from a given point to the set of training data used to take about ```1300```ms. But removing the pow function and replacing it with a basic multiplication operator, the time to iterate through all training data points, and calculating distances from the given point, now takes about ```750```ms. It takes about 35-40 minutes, and the test error rate about 2.85%.
    - Creating two threads that calculates the distance between the given point and the training data points improved performance for ```k = 1``` from ~```750```ms to ~```350```ms with similar test error rate as before. This was done by letting each thread to have their own data set to iterate through. 

## How to use for now
Currently there is only one ML algorithm, the KNN method. A typical usage looks like as follows
```cpp
#include "Core/include/Data.h"
#include "Core/include/DataDistributor.h"
#include "ML/include/KNN.h"
int main()
{
	Core::DataDistributor dataDistributor;
	dataDistributor.ReadFeatureVector("./dataset/train-images-idx3-ubyte");
	dataDistributor.ReadFeatureLabels("./dataset/train-labels-idx1-ubyte");
	dataDistributor.SetSeed(1112); // Default is set to 1111 if this is not called.
	dataDistributor.SplitData();
	dataDistributor.CountClasses();

	ML::KNN kNearest;
	kNearest.SetTrainingData(dataDistributor.GetTrainingData());
	kNearest.SetTestData(dataDistributor.GetTestData());
	kNearest.SetValidationData(dataDistributor.GetValidationData());
	kNearest.SetNumThreads(24);

	double performance = 0.0;
	double bestPerformance = 0.0;
	int bestK = 1;
	for (int i = 1; i <= 5; i++)
	{
		if (i == 1)
		{
			kNearest.SetK(i);
			performance = kNearest.ValidatePerformance();
			bestPerformance = performance;
		}
		else
		{
			kNearest.SetK(i);
			performance = kNearest.ValidatePerformance();
			if (performance > bestPerformance)
			{
				bestPerformance = performance;
				bestK = i;
			}
		}
	}

	kNearest.SetK(bestK);
	kNearest.TestPerformance();
	return 0;
}
```
The code above outputs the following result in the terminal:
```
Done getting input file header
numImages = 60000
Successfully read and stored 60000 feature vectors
Done getting label file header.
Training data size:     45000
Test data size:         12000
Validation data size:   3000
Successfully Extracted 10 unique classes.
hardware threads: 24
Number of threads set to: 24
Time passed: 175580
Validation Performance for K = 1 : 97.3333%
Time passed: 356447
Validation Performance for K = 2 : 96.3333%
Time passed: 528313
Validation Performance for K = 3 : 96.8333%
Time passed: 704506
Validation Performance for K = 4 : 96.7667%
Time passed: 882988
Validation Performance for K = 5 : 96.7333%
Time passed: 713987
Test Performance = 97.0167
```

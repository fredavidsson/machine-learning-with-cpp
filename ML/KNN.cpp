#include "include/KNN.h"

ML::KNN::KNN(int k)
	: k(k)
	, numThreads(0)
{
}

ML::KNN::KNN()
	: k(1)
	, numThreads(0)
{
}

ML::KNN::~KNN()
{
}

void ML::KNN::SetK(int val)
{
	k = val;
}

void ML::KNN::FindKNearest(Core::Data& queryPoint)
{
	neighbors = std::make_unique<DataVector>();
	double min = std::numeric_limits<double>::max();
	double previousMin = min;
	int index = 0;
	for (int i = 0; i < k; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < trainingData->size(); j++)
			{
				double distance = CalculateDistance(queryPoint, trainingData->at(j));
				trainingData->at(j).SetDistance(distance);
				if (distance < min)
				{
					min = distance;
					index = j;
				}
			}

			neighbors->push_back(trainingData->at(index));
			previousMin = min;
			min = std::numeric_limits<double>::max();
		}
		else
		{
			for (int j = 0; j < trainingData->size(); j++)
			{
				double distance = CalculateDistance(queryPoint, trainingData->at(j));
				if (distance > previousMin && distance < min)
				{
					min = distance;
					index = j;
				}
			}

			neighbors->push_back(trainingData->at(index));
			previousMin = min;
			min = std::numeric_limits<double>::max();
		}
	}
}

void ML::KNN::FindShortestDistance(
	double& threadMin,
	size_t& threadIndex,
	std::shared_ptr <DataVector> trainingData, 
	Core::Data& queryPoint,
	std::array<size_t, 2> range)
{
	// double localMin = std::numeric_limits<double>::max();
	for (size_t i = range[0]; i < range[1]; i++)
	{
		double distance = CalculateDistance(queryPoint, trainingData->at(i));
		if (distance < threadMin)
		{
			threadMin = distance;
			threadIndex = i;
		}
	}
}

void ML::KNN::FindKNearestMT(Core::Data& queryPoint)
{
	neighbors = std::make_unique<DataVector>();
	double min = std::numeric_limits<double>::max();
	double previousMin = min;
	size_t index = 0;

	std::vector<std::thread> threads(numThreads);
	std::vector<double> threadMins(numThreads);
	std::vector<size_t> threadIndexes(numThreads);
	std::vector<std::array<size_t, 2>> threadRanges(numThreads);

	// Give each thread a partition of the training set to work on
	const size_t size = trainingData->size();
	for (size_t j = 0; j < numThreads; j++)
	{
		size_t M = size / numThreads * (j + 1);
		size_t N = size / numThreads * j;
		threadRanges[j] = { N, M };
	}

	for (int i = 0; i < k; i++)
	{
		if (i == 0)
		{
			for (size_t j = 0; j < numThreads; j++)
			{
				threadMins[j] = std::numeric_limits<double>::max();
				threads[j] = std::thread(
					&ML::KNN::FindShortestDistance, this,
					std::ref(threadMins[j]),
					std::ref(threadIndexes[j]),
					trainingData,
					std::ref(queryPoint),
					threadRanges[j]);
			}

			for (size_t j = 0; j < numThreads; j++)
			{
				if (threads[j].joinable())
				{
					threads[j].join();
				}

				if (threadMins[j] < min)
				{
					min = threadMins[j];
					index = threadIndexes[j];
				}
			}

			neighbors->push_back(trainingData->at(index));
			previousMin = min;
			min = std::numeric_limits<double>::max();
		}
		else
		{

			for (int j = 0; j < numThreads; j++)
			{
				threadMins[j] = std::numeric_limits<double>::max();
				threads[j] = std::thread(
					&ML::KNN::FindShortestDistance, this,
					std::ref(threadMins[j]),
					std::ref(threadIndexes[j]),
					trainingData,
					std::ref(queryPoint),
					threadRanges[j]);
			}

			for (int j = 0; j < numThreads; j++)
			{
				if (threads[j].joinable())
				{
					threads[j].join();
				}

				if (threadMins[j] > previousMin && threadMins[j] < min)
				{
					min = threadMins[j];
					index = threadIndexes[j];
				}
			}

			neighbors->push_back(trainingData->at(index));
			previousMin = min;
			min = std::numeric_limits<double>::max();
		}
	}
}

void ML::KNN::SetNumThreads(const unsigned int val)
{
	if (val < 1)
	{
		std::cout
			<< "ML::KNN::SetNumThreads needs a value greater or equal to 1."
			<< "\n" << "Running on one thread instead."
			<< std::endl;

		numThreads = 1;
	}
	else 
	{
		numThreads = val;
	}

	auto hardwareThreads = std::thread::hardware_concurrency();
	std::cout
		<< "hardware threads: " << hardwareThreads << '\n'
		<< "Number of threads set to: " << numThreads 
		<< std::endl;

	if (val > hardwareThreads && hardwareThreads != 0)
	{
		numThreads = 1;
		std::cout
			<< "Oversubscription error: "
			<< "You don't want to run more threads than the hardware can support\n"
			<< "Number of thread set to " << numThreads << " instead"
			<< std::endl;
	}
}

void ML::KNN::SetTrainingData(std::shared_ptr<DataVector> vector)
{
	trainingData = vector;
}

void ML::KNN::SetTestData(std::shared_ptr<DataVector> vector)
{
	testData = vector;
}

void ML::KNN::SetValidationData(std::shared_ptr<DataVector> vector)
{
	validationData = vector;
}

int ML::KNN::Predict()
{
	std::map<uint16_t, int> classFrequency;
	for (int i = 0; i < neighbors->size(); i++)
	{
		if (classFrequency.find(neighbors->at(i).GetLabel()) == classFrequency.end())
		{
			classFrequency[neighbors->at(i).GetLabel()] = 1;
		}
		else
		{
			classFrequency[neighbors->at(i).GetLabel()]++;
		}
	}

	int best = 0;
	int max = 0;
	for (auto& kv : classFrequency)
	{
		if (kv.second > max)
		{
			max = kv.second;
			best = kv.first;
		}
	}

	neighbors.reset();
	return best;
}

double ML::KNN::CalculateDistance(Core::Data& queryPoint, Core::Data& input)
{
	double distance = 0.0;

	assert(
		(queryPoint.GetFeatureVectorSize() == input.GetFeatureVectorSize() 
		&& "Error: Vector size mismatch"));

	for (size_t i = 0; i < queryPoint.GetFeatureVectorSize(); i++)
	{
		const uint8_t x = queryPoint.GetFeatureVector().at(i);
		const uint8_t y = input.GetFeatureVector().at(i);
		const double n = x - y;
		distance += n * n;
	}

	distance = std::sqrt(distance);
	return distance;
}

double ML::KNN::ValidatePerformance()
{
	double validationPerformance = 0.0;
	int count = 0;
	int dataIndex = 0;
	const size_t size = validationData->size();

	progressBar.SetMax(size);
	progressBar.SetFraction(size * 0.1);

	auto tStart = std::chrono::high_resolution_clock::now();
	for (Core::Data& queryPoint : *validationData)
	{
		if (numThreads > 1)
		{
			FindKNearestMT(queryPoint);
		} 
		else
		{
			FindKNearest(queryPoint);
		}

		int prediction = Predict();
		if (prediction == queryPoint.GetLabel())
		{
			count++;
		}

		dataIndex++;
		progressBar.Update(); // Only runs when enabled.
	}
	auto tEnd = std::chrono::high_resolution_clock::now();
	validationPerformance = count * 100 / static_cast<double>(size);
	std::cout
		<< "Time passed: "
		<< std::chrono::duration<double, std::milli>(tEnd - tStart).count()
		<< '\n'
		<< "Validation Performance for K = " << k << " : "
		<< validationPerformance << "%"
		<< std::endl;

	progressBar.ResetProgress();

	return validationPerformance;
}

double ML::KNN::TestPerformance()
{
	double testPerformance = 0.0;
	int count = 0;
	const size_t size = testData->size();
	
	progressBar.SetMax(size);
	progressBar.SetFraction(size * 0.05);

	auto tStart = std::chrono::high_resolution_clock::now();
	for (Core::Data& queryPoint : *testData)
	{
		FindKNearestMT(queryPoint);
		int prediction = Predict();
		if (prediction == queryPoint.GetLabel())
		{
			count++;
		}

		progressBar.Update();
	}

	auto tEnd = std::chrono::high_resolution_clock::now();
	testPerformance = count * 100 / static_cast<double>(testData->size());
	std::cout 
		<< "Time passed: "
		<< std::chrono::duration<double, std::milli>(tEnd - tStart).count() 
		<< '\n'
		<< "Test Performance = " 
		<< testPerformance 
		<< std::endl;

	progressBar.ResetProgress();
	return testPerformance;
}
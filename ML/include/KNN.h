#pragma once
#include<vector>
#include<memory>
#include<cmath>
#include<limits>
#include<map>
#include<thread>
#include<cassert>
#include<chrono>
#include<algorithm>
#include<iostream>
#include"../../Core/include/Data.h"
#include"../../Core/include/DataDistributor.h"
#include"../../Core/include/ProgressBar.h"
namespace ML
{
	class KNN
	{
	public:
		typedef std::vector<Core::Data> DataVector;

		KNN(int k);
		KNN();
		~KNN();

		void FindKNearest(Core::Data& queryPoint);
		void FindKNearestMT(Core::Data& queryPoint); // Multithreaded version
		void SetNumThreads(const unsigned int val);

		void SetTrainingData(std::shared_ptr<DataVector> vector);
		void SetTestData(std::shared_ptr<DataVector> vector);
		void SetValidationData(std::shared_ptr<DataVector> vector);
		void SetK(int val);

		int Predict();

		double CalculateDistance(Core::Data& queryPoint, Core::Data& input);
		double ValidatePerformance();
		double TestPerformance();

		Core::ProgressBar progressBar;
	private:
		int k;
		std::unique_ptr<DataVector> neighbors;
		
		// multithreading  -----------------------------------------------------
		int numThreads = 1;
		void FindShortestDistance(double& min,
			size_t& threadIndex,
			std::shared_ptr <ML::KNN::DataVector> trainingData,
			Core::Data& queryPoint,
			std::array<size_t, 2> range);
		// ---------------------------------------------------------------------

		std::shared_ptr<DataVector> trainingData;
		std::shared_ptr<DataVector> testData;
		std::shared_ptr<DataVector> validationData;

		
	};
}

